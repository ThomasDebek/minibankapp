module ApplicationHelper
  def money(amount, currency='zl')
    "#{amount} #{currency}"
  end
end
